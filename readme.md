# Minecraft 1.18.2 Forge server installer /w Ansible

Only works with apt-based Linux distributions cause I'm too lazy to add yum or pacman support

Usage: 
* git clone https://gitlab.com/vikman_/netcracker-ansible-task.git
* cd netcracker-ansible-task
* ansible-playbook minecraft_playbook.yml -i ./hosts --extra-vars "install_path=x", where x is obviously your desired installation path
* run run.sh script to start the server